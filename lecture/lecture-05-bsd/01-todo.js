document.title = "TODO-Liste";

let input = document.querySelector('input[name="eintrag"]');
let note = document.querySelector('textarea[name="note"]');
let date = document.querySelector('input[name="date"]');
let button = document.getElementById('btn-eintrag');
let todo = document.querySelector('#todo');
let info_area = document.getElementById('info_area');

const url = "https://xn--4ca9a.eu:8090/todo/";

fetch(url).then((res) => {
  if(res.ok){
    return res.json();
  }
}).then((data) => {
  for(var i = 0; i < data.length; i++)
  {
    console.log(data);
    var lst_element = document.createElement('li');
    lst_element.innerText = data[i].name +  (data[i].date != null ? " ("+data[i].date+")" : '') ;
    lst_element.className = 'eintrag list-group-item';
    lst_element.id = data[i].id;
    todo.appendChild(lst_element);
  
    input.value = ''; ;
  }
  
  [...todo.children].forEach((elem, index) => {
    console.log(elem);
  });

}).catch((err) => {

});


button.addEventListener('click', function(event){
  event.preventDefault();
  input.style = ""; //Reset input

  if(input.value != '')
  {
    let todo_data = {
      name: input.value,
      note: note.value,
      date: date.value
    }

    console.log(JSON.stringify(todo_data))

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },      
      body: JSON.stringify(todo_data)
    }).then(response => response.json())
    .then(data => {
      console.log(data);
    })
    .catch((error) => {
      info_area.innerHTML = error;
    })

    var lst_element = document.createElement('li');
    lst_element.innerText = input.value;
    lst_element.className = 'eintrag list-group-item';

    todo.appendChild(lst_element);

    info_area.innerHTML = "Eintrag wurde hinzugef&uuml;gt!";
    info_area.className  = "text-success";

    input.value = ''; 
    note.value = '';
    date.value = '';
  }
  else 
  {
    input.style = "background-color: #e89b9b";
    info_area.innerHTML = "Aufgabe eingeben!";
    info_area.className  = "text-danger";
  }
});


todo.addEventListener('dblclick', function(event){
    fetch(url + "/" + event.target.id, {
      method: 'DELETE'
    }).then(response => {
      if(response.status == 204)
      {
        info_area.innerHTML = "Eintrag wurde gelöscht!";
        info_area.className  = "text-success";
      }
    })
    .catch((error) => {
      console.log(error)
    })

  todo.removeChild(event.target);


});

