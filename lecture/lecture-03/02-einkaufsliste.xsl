<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/einkaufsliste">
    <html>
      <head>
        <style>
          li {
            font-size: 25px;
          }
        </style>
      </head>
      <body>
        <h1>Unsere Einkaufsliste</h1>
        <p>Bitte kauf uns doch ein:</p>
        <ul>
          <xsl:for-each select="eintrag">
            <xsl:sort select="name" order="ascending"></xsl:sort>
            <li><xsl:value-of select="menge" /> <xsl:value-of select="name" /> </li>
          </xsl:for-each>
        </ul>

        <xsl:choose>
          <xsl:when test="count(//eintrag)=1">
            	<p>Unsere Einkaufsliste hat ein <xsl:value-of select="count(//eintrag)" /> Element.</p>
          </xsl:when>
          <xsl:when test="count(//eintrag)=10">
            <p>Unsere Einkaufsliste hat ein <xsl:value-of select="count(//eintrag)" /> Element.</p>
            <p>Bitte Einkaufssackerl nicht vergessen!</p>
          </xsl:when>
          <xsl:otherwise>
            <p>Unsere Einkaufsliste hat <xsl:value-of select="count(//eintrag)" /> Elemente.</p>
          </xsl:otherwise>
        </xsl:choose>
<!--
        <xsl:if test="count(//eintrag)=1">
          <p>Unsere Einkaufsliste hat ein <xsl:value-of select="count(//eintrag)" /> Element.</p>
        </xsl:if>

        <xsl:if test="count(//eintrag)> 1">
          <p>Unsere Einkaufsliste hat <xsl:value-of select="count(//eintrag)" /> Elemente.</p>
        </xsl:if>
      -->
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>