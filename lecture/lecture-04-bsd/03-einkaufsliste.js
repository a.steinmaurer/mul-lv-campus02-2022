document.title = "Einkaufsliste";

const input = document.querySelector('input[name="eintrag"]');
const button = document.getElementById('btn-eintrag');
const einkaufsliste = document.querySelector('#einkaufsliste');
const suche = document.querySelector('input[name="suche"]');

for(let i = 0; i < data.einkaufsliste.length; i++)
{
  console.log(data);
  const lst_element = document.createElement('li');
  lst_element.innerText = data.einkaufsliste[i].name;
  lst_element.className = 'eintrag list-group-item';
  einkaufsliste.appendChild(lst_element);

  input.value = ''; ;
}

//Spread-Syntax, da HTMLCollection durch die mit forEach nicht iteriert werden kann (weil nicht Array).
//Bei childNodes werden Zeilenumbrüche auch mitgegeben, also lieber children
[...einkaufsliste.children].forEach((elem, index) => {
  console.log(elem);
});

button.addEventListener('click', function(event){
  event.preventDefault();
  input.style = ""; //Reset input

  if(input.value != '')
  {
    const lst_element = document.createElement('li');
    lst_element.innerText = input.value;
    lst_element.className = 'eintrag list-group-item';

    einkaufsliste.appendChild(lst_element);

    input.value = ''; 
  }
  else 
  {
    input.style = "background-color: #e89b9b";
  }
});

einkaufsliste.addEventListener('dblclick', function(event){
  einkaufsliste.removeChild(event.target);
});

//keyup vs keydown
suche.addEventListener('keyup', function(event){
  const eintraege = einkaufsliste.getElementsByTagName('li');

  [...eintraege].forEach(function(eintrag){
    if(eintrag.innerText.toLowerCase().indexOf(event.target.value.toLowerCase()) != -1)
    {
      eintrag.style = "display: block";
    } 
    else 
    {
      eintrag.style = "display: none";
    }
  });
    
});
