console.log(document)
console.dir(document)

document.title = "Einkaufsliste";

var req = fetch("https://xn--4ca9a.eu/FH/einkaufsliste.json");

console.log(req);

var input = document.querySelector('input[name="eintrag"]');
var button = document.getElementById('btn-eintrag');
var einkaufsliste = document.querySelector('#einkaufsliste');
var suche = document.querySelector('input[name="suche"]');

req.then((res) => {
  if(res.ok){
    return res.json();
  }
}).then((data) => {
console.log(data);
  for(var i = 0; i < data.einkaufsliste.length; i++)
  {
    console.log(data);
    var lst_element = document.createElement('li');
    lst_element.innerText = data.einkaufsliste[i].name;
    lst_element.className = 'eintrag list-group-item';
    einkaufsliste.appendChild(lst_element);
  
    input.value = ''; ;
  }
  
  //Spread-Syntax, da HTMLCollection durch die mit forEach nicht iteriert werden kann (weil nicht Array).
  //Bei childNodes werden Zeilenumbrüche auch mitgegeben, also lieber children
  [...einkaufsliste.children].forEach((elem, index) => {
    console.log(elem);
  });

}).catch((err) => {

});


button.addEventListener('click', function(event){
  event.preventDefault();
  input.style = ""; //Reset input

  if(input.value != '')
  {
    var lst_element = document.createElement('li');
    lst_element.innerText = input.value;
    lst_element.className = 'eintrag list-group-item';

    einkaufsliste.appendChild(lst_element);

    input.value = ''; 
  }
  else 
  {
    input.style = "background-color: #e89b9b";
  }
});

einkaufsliste.addEventListener('dblclick', function(event){
  einkaufsliste.removeChild(event.target);
});

//keyup vs keydown
suche.addEventListener('keyup', function(event){
  var eintraege = einkaufsliste.getElementsByTagName('li');

  [...eintraege].forEach(function(eintrag){
    if(eintrag.innerText.toLowerCase().indexOf(event.target.value.toLowerCase()) != -1)
    {
      eintrag.style = "display: block";
    } 
    else 
    {
      eintrag.style = "display: none";
    }
  });
    
});
